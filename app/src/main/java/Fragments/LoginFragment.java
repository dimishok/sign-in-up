package Fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dimishok.regfragments.DBHelper;
import com.dimishok.regfragments.R;
import com.dimishok.regfragments.SuccessActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends Fragment {
    @BindView(R.id.SignInLogin) Button SignInLogin;
    @BindView(R.id.SignUpLogin) Button SignUpLogin;
    @BindView(R.id.ETEmailLogin) EditText ETEmailLogin;
    @BindView(R.id.ETPassLogin) EditText ETPassLogin;
    @BindView(R.id.ETEmailLoginLayout) TextInputLayout ETEmailLoginLayout;
    @BindView(R.id.ETPassLoginLayout) TextInputLayout ETPassLoginLayout;


    ContentValues cv;
    DBHelper dbHelper;
    SQLiteDatabase db;
    Context context;
    FragmentManager fragmentManager;

    @OnClick(R.id.SignInLogin)
    public void SignInLoginClick(){
        if(ETEmailLogin.getText().toString().isEmpty()){
            ETEmailLogin.setError(getResources().getString(R.string.ErrorEmpty));
            return;
        }
        if(ETPassLogin.getText().toString().isEmpty()){
            ETPassLogin.setError(getResources().getString(R.string.ErrorEmpty));
            return;
        }
            Cursor c = db.rawQuery("SELECT * FROM users ;", null);
            c.moveToFirst();
            String pass = "";
            boolean isFind = false;
        if (c.moveToFirst()){
            do {
                String column2 = c.getString(1);
               if(ETEmailLogin.getText().toString().equals(column2)) {
                   pass = c.getString(2);
                   isFind=true;
               }

            } while(c.moveToNext());
        }
            c.close();
        if(!isFind){
            ETEmailLogin.setError(getResources().getString(R.string.ErrorInvalidLogin));

        }
        else {

            if (!pass.equals(ETPassLogin.getText().toString())) {
                ETPassLogin.setError(getResources().getString(R.string.ErrorInvalidPass));

            } else {

                Toast.makeText(context,"TRUE",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, SuccessActivity.class);
                startActivity(intent);
            }
        }



    }


    @OnClick(R.id.SignUpLogin)
    public void SignUpLoginClick(){

        Fragment fragment ;
        fragment = new RegFragment();
        try {
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();
        }
        catch (Exception ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        context = getContext();
        cv = new ContentValues();
        dbHelper  = new DBHelper(getContext());
        db = dbHelper.getReadableDatabase();
        ButterKnife.bind(this,view );
        fragmentManager = getFragmentManager();
        return view;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }
}
