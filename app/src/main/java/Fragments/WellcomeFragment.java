package Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dimishok.regfragments.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WellcomeFragment extends Fragment {

    @BindView(R.id.SignInWellcom)  Button SignInButton;
    @BindView(R.id.SignUpWellcom)  Button SignUpButton;







    @OnClick(R.id.SignInWellcom)
    public void SignIn() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment);
        if(fragment==null){
            fragment = new LoginFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).addToBackStack(null).commit();
        }
        else
        {
            fragment = new LoginFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).addToBackStack(null).commit();
        }




    }

    @OnClick(R.id.SignUpWellcom)
    public void SignUp() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment, new RegFragment()).addToBackStack(null).commit();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wellcome, container, false);

        ButterKnife.bind(this, view);
        return view;

    }



}
