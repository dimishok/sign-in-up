package Fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dimishok.regfragments.DBHelper;
import com.dimishok.regfragments.R;
import com.dimishok.regfragments.SuccessActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegFragment extends Fragment {
    @BindView(R.id.SignUpReg) Button SignUpReg;
    @BindView(R.id.ETMail) EditText ETEmail;
    @BindView(R.id.ETPass) EditText ETPass;
    @BindView(R.id.ETConfirm) EditText ETConfirm;
    @BindView(R.id.ETMailLayout) TextInputLayout  ETMailLayout;
    @BindView(R.id.ETPassLayout) TextInputLayout ETPassLayout;
    @BindView(R.id.ETConfirmLayout) TextInputLayout  ETConfirmLayout;
    ContentValues cv;
    DBHelper dbHelper;
    SQLiteDatabase db;
    Context context;



    private  boolean ETMailCheck(){
        if(ETEmail.getText().toString().equals("")) {
            ETMailLayout.setErrorEnabled(true);
            ETMailLayout.setError(getResources().getString(R.string.ErrorEmpty));
            return false;
        }
        else {
            ETMailLayout.setErrorEnabled(false);
            if (ETEmail.getText().toString().length() < 8) {
                ETMailLayout.setErrorEnabled(true);
                ETMailLayout.setError(getResources().getString(R.string.ErrorTooShort));
                return false;
            }
            else {
                ETMailLayout.setErrorEnabled(false);
                return true;
            }
        }
    }

    private  boolean ETPassCheck(){
        if(ETPass.getText().toString().equals("")) {
            ETPassLayout.setErrorEnabled(true);
            ETPassLayout.setError(getResources().getString(R.string.ErrorEmpty));
            return false;
        }
        else {
            ETPassLayout.setErrorEnabled(false);
            if (ETPass.getText().toString().length() < 8) {
                ETPassLayout.setErrorEnabled(true);
                ETPassLayout.setError(getResources().getString(R.string.ErrorTooShort));
                return false;
            }
            else {
                ETPassLayout.setErrorEnabled(false);
                return true;
            }

        }


    }
    private  boolean ETConfirmCheck() {
        if (ETConfirm.getText().toString().equals("")) {
            ETConfirmLayout.setError(getResources().getString(R.string.ErrorEmpty));
            return false;
        } else {
            ETConfirmLayout.setErrorEnabled(false);
            if (ETConfirm.getText().toString().length() < 8) {
                ETConfirmLayout.setError(getResources().getString(R.string.ErrorTooShort));
                return false;
            } else {
                ETConfirmLayout.setErrorEnabled(false);
                if (!ETPass.getText().toString().equals(ETConfirm.getText().toString())) {
                    ETConfirmLayout.setError(getResources().getString(R.string.ErrorPassDiff));
                    return false;
                } else {
                    return true;
                }


            }

        }

    }


    @OnClick(R.id.SignUpReg)
    public void SignUpRegCkick(){
        if(ETMailCheck())
            if(ETPassCheck())
                if(ETConfirmCheck()){

                    cv.put("userslogin",ETEmail.getText().toString());
                    cv.put("userspass",ETPass.getText().toString());
                    long rowID = db.insert("users", null, cv);
                    Log.d(RegFragment.class.getSimpleName(), "row inserted, ID = " + rowID);

                    Intent intent = new Intent(context, SuccessActivity.class);
                    startActivity(intent);
                }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_reg, container, false);

        ButterKnife.bind(this, view);
        context = getContext();
        cv = new ContentValues();
        dbHelper  = new DBHelper(getContext());
        db = dbHelper.getWritableDatabase();
        return view;
    }


}
