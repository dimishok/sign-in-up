package com.dimishok.regfragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import Fragments.LoginFragment;
import Fragments.WellcomeFragment;

public class MainActivity extends  FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        fragment = new LoginFragment();
        fragmentManager.beginTransaction().add(R.id.fragment,fragment).commit();
    }
}
